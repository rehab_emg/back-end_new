﻿using Jose;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace API.Security
{
    public class JwtAuthUtil
    {
        public string GenerateToken(string account)
        {
            //加解密的key (如果不一樣會無法成功解密)
            string secret = ConfigurationManager.AppSettings["secretKey"];

            //payload
            Dictionary<string, object> claim = new Dictionary<string, object>
            {
                { "info", account },
                { "exp", DateTime.Now.AddDays(Convert.ToInt32("30")).ToString() }
            };
            var payload = claim;

            //產生token
            var token = JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS512);

            return token;  //Header + Payload + Signature
        }
    }
}