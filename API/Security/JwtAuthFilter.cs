﻿using Jose;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace API.Security
{
    public class JwtAuthFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //加解密的key (如果不一樣會無法成功解密)
            string secret = ConfigurationManager.AppSettings["secretKey"];
            var request = actionContext.Request;

            try
            {
                if (request.Headers.Authorization == null || request.Headers.Authorization.Scheme != "Bearer")
                {
                    throw new Exception("Lost Token");
                }
                else
                {
                    var jwtObject = JWT.Decode<Dictionary<string, object>>(request.Headers.Authorization.Parameter,
                                    Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS512);

                    if (IsTokenExpired(jwtObject["exp"].ToString()))
                    {
                        throw new Exception("Expired Token");
                    }
                }
            }
            catch
            {
                throw new Exception("Wrong Token");
            }

            base.OnActionExecuting(actionContext);
        }

        #region 驗證 token 時效
        public bool IsTokenExpired(string dateTime)
        {
            return Convert.ToDateTime(dateTime) < DateTime.Now;
        }
        #endregion
    }
}
