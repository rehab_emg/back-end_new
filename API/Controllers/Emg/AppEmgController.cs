﻿using API.Models;
using API.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/appemg")]
    public class AppEmgController : ApiController
    {
        private InsertEmgService _insertService = new InsertEmgService();
        private EmgService emgService = new EmgService();
        private AnalysisService analysisService = new AnalysisService();

        #region 接收 sEMG (前測)
        [HttpPost]
        [Route("insertpre")]
        public HttpResponseMessage GetEmg_Pre([FromBody]SemgData data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (data.account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    _insertService.GetData(data, 1);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e);
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 接收 sEMG (檢測中)
        [HttpPost]
        [Route("insert")]
        public HttpResponseMessage GetEmg([FromBody]SemgData data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (data.account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    _insertService.GetData(data, 2);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e);
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 接收 sEMG (後測)
        [HttpPost]
        [Route("insertpost")]
        public HttpResponseMessage GetEmg_Post([FromBody]SemgData data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (data.account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    _insertService.GetData(data, 3);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e);
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 分析用 (暫定)
        [HttpPost]
        [Route("calculate")]
        public HttpResponseMessage Analysis_FFTScore([FromBody]DetectionId data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            try
            {
                _insertService.Calculate(data.dt_id);
                analysisService.CalculateScore(data.dt_id);
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e);
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 取得User's 全部檢測時間
        [HttpPost]
        [Route("recordtime")]
        public HttpResponseMessage GetLastRecord([FromBody]Account data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();

            if (data.account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    info = emgService.GetDetectionTime(data.account);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e.Message);
                }
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得User's 歷史紀錄
        [HttpPost]
        [Route("hisrecord")]
        public HttpResponseMessage GetHistoricalRecord_dt_id([FromBody]DetectionId data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();

            try
            {
                info = emgService.GetHistoricalData_dt(data);
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e.Message);
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion
    }
}