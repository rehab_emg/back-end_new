﻿using API.Models;
using API.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/emg")]
    public class EmgController : ApiController
    {
        private EmgService emgService = new EmgService();
        private AnalysisService analysisService = new AnalysisService();

        #region 取得User's 最後一筆紀錄
        [HttpGet]
        [Route("lastrecord")]
        public HttpResponseMessage GetLastRecord()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();
            string account = Request.Headers.Contains("account") ? Request.Headers.GetValues("account").First() : null;

            if (account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    info = emgService.GetUserData(account);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e.Message);
                }
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得User's 歷史紀錄(5筆)
        [HttpGet]
        [Route("hisrecord5")]
        public HttpResponseMessage GetHistoricalRecord_5()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();
            string account = Request.Headers.Contains("account") ? Request.Headers.GetValues("account").First() : null;

            if (account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    info = emgService.GetHistoricalData(emgService.GetDetectionId(account, 5));
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e.Message);
                }
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得User's 歷史紀錄(時間區間)
        [HttpPost]
        [Route("hisrecordtime")]
        public HttpResponseMessage GetHistoricalRecord_Time([FromBody]RecordTime data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();

            if (data.account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    info = emgService.GetHistoricalData(emgService.GetDetectionId_Time(data));
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e.Message);
                }
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得User's 最後一筆的肌肉萎縮程度評估
        [HttpGet]
        [Route("lastevaluation")]
        public HttpResponseMessage GetLastEvaluation()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();
            string account = Request.Headers.Contains("account") ? Request.Headers.GetValues("account").First() : null;

            if (account == null)
                code = (HttpStatusCode)401;
            else
            {
                try
                {
                    info = emgService.GetEvaluation_Last(account);
                }
                catch (Exception e)
                {
                    code = (HttpStatusCode)500;
                    result.Add("message", e.Message);
                }
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));
        }
        #endregion

        #region 取得部位檢測次數
        [HttpGet]
        [Route("parttimes")]
        public HttpResponseMessage GetPartFrequency()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JArray info = new JArray();

            try
            {
                info = emgService.GetPartFrequency();
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e.Message);
            }
            return Request.CreateResponse(code, info ?? JArray.FromObject(result));  //如果info == null -> JArray.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得左右腳檢測次數
        [HttpGet]
        [Route("legtimes")]
        public HttpResponseMessage GetLegFrequency()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JObject info = new JObject();

            try
            {
                info = emgService.GetLegFrequency();
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e.Message);
            }
            return Request.CreateResponse(code, info ?? JObject.FromObject(result));  //如果info == null -> JObject.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得年齡分布
        [HttpGet]
        [Route("agetimes")]
        public HttpResponseMessage GetAgeFrequency()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JObject info = new JObject();

            try
            {
                info = emgService.GetAgeFrequency();
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e.Message);
            }
            return Request.CreateResponse(code, info ?? JObject.FromObject(result));  //如果info == null -> JObject.FromObject(result) 否則 return info
        }
        #endregion

        #region 取得性別分布
        [HttpGet]
        [Route("sextimes")]
        public HttpResponseMessage GetSexFrequency()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            JObject info = new JObject();

            try
            {
                info = emgService.GetSexFrequency();
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                result.Add("message", e.Message);
            }
            return Request.CreateResponse(code, info ?? JObject.FromObject(result));  //如果info == null -> JObject.FromObject(result) 否則 return info
        }
        #endregion
    }
}