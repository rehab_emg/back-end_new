﻿using API.Models;
using API.Security;
using API.Services;
using Jose;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace API.Controllers
{
    [RoutePrefix("api/member")]
    public class MemberController : ApiController
    {
        private rehabEntities db = new rehabEntities();
        private MemberService memberService = new MemberService();
        private MailService mailService = new MailService();

        #region 取得 UserInfo
        [HttpGet]
        [Route("userinfo")]
        [JwtAuthFilter]
        public HttpResponseMessage UserInfo()
        {
            var header = Request.Headers.Authorization;
            var secret = Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["secretKey"]);

            if (header != null)
            {
                var token = JWT.Decode<Dictionary<string, object>>(header.Parameter, secret, JwsAlgorithm.HS512);
                var result = memberService.getById(token["info"].ToString());

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized, new Dictionary<string, object>());
        }
        #endregion

        #region 取得 UserInfo (Android)
        [HttpPost]
        [Route("user")]
        public HttpResponseMessage UserInfoByApp([FromBody]Account data)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            try
            {
                if (data.account != null)
                {
                    var result = memberService.getById(data.account);
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                code = (HttpStatusCode)401;
            }
            catch (Exception e)
            {
                code = (HttpStatusCode)500;
                dic.Add("message", e.Message);
            }
            return Request.CreateResponse(code, dic);
        }
        #endregion

        #region 登入
        [HttpPost]
        [Route("login")]
        public HttpResponseMessage Login([FromBody]MemberLogin data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;
            bool check = memberService.Login(data.account, memberService.HashPassword(data.password));
            bool check_ac = memberService.AuthcodeCheck(data.account);

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                if (check_ac == false)  //信箱未驗證 / 忘記密碼後未重設
                    code = (HttpStatusCode)402;
                else
                {
                    if (check == false)  // 帳密錯誤
                        code = (HttpStatusCode)401;
                    else
                    {
                        JwtAuthUtil jwtAuthUtil = new JwtAuthUtil();
                        string jwtToken = jwtAuthUtil.GenerateToken(data.account);

                        result.Add("token", jwtToken);
                    }
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 註冊
        [HttpPost]
        [Route("register")]
        public HttpResponseMessage Register([FromBody]member data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                if (memberService.AccountCheck(data.account) == false)  //檢查是否有重複帳號            
                    code = (HttpStatusCode)401;
                else
                {
                    data.authcode = mailService.GetValidateCode(true);  //產生authcode
                    SendEmail(data.account, data.email, data.authcode, true);  //寄信
                    memberService.Register(data);  //註冊
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 修改

        #region 修改密碼
        [HttpPost]
        [Route("changepassword")]
        [JwtAuthFilter]
        public HttpResponseMessage ChangePassword([FromBody]MemberPassword data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                //檢查輸入的舊密碼與資料庫裡的是否相同
                if (memberService.CheckPassword(data.account, data.password) == false)
                    code = (HttpStatusCode)401;  //輸入的舊密碼與資料庫不同
                else
                    memberService.ChangePassword(data.account, data.newPassword);
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 修改姓名 電話 Email 大頭貼
        [HttpPost]
        [Route("changeother")]
        [JwtAuthFilter]
        public HttpResponseMessage ChangeOther([FromBody]MemberChange data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
                code = (HttpStatusCode)memberService.ChangeImage(data);

            return Request.CreateResponse(code, result);
        }
        #endregion

        #endregion

        #region 忘記密碼 && 重設密碼

        #region 忘記密碼
        [HttpPost]
        [Route("forgetpassword")]
        public HttpResponseMessage ForgetPassword([FromBody]MemberForget data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                string authcode = mailService.GetValidateCode(false);
                bool check = memberService.ForgetPasswordCheck(data.account, data.email, authcode);

                if (check == false)
                    code = (HttpStatusCode)402;
                else
                    SendEmail(data.account, data.email, authcode, false);
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 重設密碼
        [HttpPost]
        [Route("resetpassword")]
        public HttpResponseMessage ResetPassword([FromBody]MemberReset data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                bool check = memberService.ResetPassword(data.authcode, data.password);

                if (check == false)
                    code = (HttpStatusCode)401;
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #endregion

        #region 信箱驗證
        [HttpGet]
        [Route("emailvalidate")]
        public IHttpActionResult EmailValidate([FromUri]string AC, [FromUri]string AU)
        {
            bool check = memberService.EmailValidate(AC, AU, 1);

            if (check == false)
                return Redirect("http://8th2.hsclab.nutc.edu.tw/#/member/validation/fail");  //失敗 or 已驗證 

            return Redirect("http://8th2.hsclab.nutc.edu.tw/#/member/validation/success");
        }
        #endregion

        #region 寄信
        public void SendEmail(string account, string email, string authcode, bool x)  //true --> 信箱驗證信  false --> 重設密碼信
        {
            string path = "";
            string url = (x == true) ? "~/EmailTemplate/Register.html" : "~/EmailTemplate/ForgetPassword.html";

            string rootPath = System.Web.Hosting.HostingEnvironment.MapPath(url);  //相對路徑
            string tempMail = File.ReadAllText(rootPath);  //讀檔

            if (x == true)
            {
                UriBuilder validateUrl = new UriBuilder(Request.RequestUri)
                {
                    Path = Url.Link("EmailApi", new { AC = memberService.UrlEncode(account), AU = memberService.UrlEncode(authcode) })
                };
                path = validateUrl.Path.Replace("%3F", "?");  //信件路徑
            }
            else
                path = "http://8th2.hsclab.nutc.edu.tw/#/member/resetpassword";

            string mailBody = mailService.GetMailBody(tempMail, account, authcode, path, x);  //信件內容

            mailService.SendEmail(email, mailBody, x);  //寄信
        }
        #endregion
    }
}