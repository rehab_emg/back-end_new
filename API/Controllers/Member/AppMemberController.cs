﻿using API.Models;
using API.Services;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers.Member
{
    [RoutePrefix("api/appmember")]
    public class AppMemberController : ApiController
    {
        private rehabEntities db = new rehabEntities();
        private MemberService memberService = new MemberService();
        private MailService mailService = new MailService();

        #region 註冊
        [HttpPost]
        [Route("register")]
        public HttpResponseMessage Register([FromBody]member data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)  //格式驗證錯誤
                code = (HttpStatusCode)403;
            else
            {
                if (memberService.AccountCheck(data.account) == false)  //檢查是否有重複帳號            
                    code = (HttpStatusCode)401;
                else
                {
                    data.authcode = mailService.GetValidateCode(false);  //產生authcode (產生出6碼的驗證碼 for APP)
                    SendEmail(data.account, data.email, data.authcode, false);  //寄信 (填寫驗證範本時)
                    memberService.Register(data);  //註冊
                }
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 修改

        #region 修改密碼
        [HttpPost]
        [Route("changepassword")]
        public HttpResponseMessage ChangePassword([FromBody]MemberPassword data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)
                code = (HttpStatusCode)403;
            else
            {
                //檢查輸入的舊密碼與資料庫裡的是否相同
                if (memberService.CheckPassword(data.account, data.password) == false)
                    code = (HttpStatusCode)401;  //輸入的舊密碼與資料庫不同
                else
                    memberService.ChangePassword(data.account, data.newPassword);
            }
            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 修改姓名 電話 Email
        [HttpPost]
        [Route("changeother")]
        public HttpResponseMessage ChangeOther([FromBody]MemberChange_APP data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            if (!ModelState.IsValid)
                code = (HttpStatusCode)403;
            else
                memberService.ChangeOther_APP(data);

            return Request.CreateResponse(code, result);
        }
        #endregion

        #endregion

        #region 信箱驗證
        [HttpPost]
        [Route("emailvalidate")]
        public HttpResponseMessage EmailValidate(MemberValidate_APP data)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            HttpStatusCode code = (HttpStatusCode)200;

            bool check = memberService.EmailValidate(data.account, data.authcode, 2);

            if (check == false)
                code = (HttpStatusCode)401;

            return Request.CreateResponse(code, result);
        }
        #endregion

        #region 寄信
        public void SendEmail(string account, string email, string authcode, bool x)  //false --> 信箱驗證信 (填寫驗證範本時)
        {
            string path = "";
            string url = "~/EmailTemplate/Register_APP.html";

            string rootPath = System.Web.Hosting.HostingEnvironment.MapPath(url);  //相對路徑
            string tempMail = File.ReadAllText(rootPath);  //讀檔
            string mailBody = mailService.GetMailBody(tempMail, account, authcode, path, x);  //信件內容

            mailService.SendEmail(email, mailBody, true);  //寄信 (在寄信時，x = true 主旨為會員註冊驗證信)
        }
        #endregion
    }
}