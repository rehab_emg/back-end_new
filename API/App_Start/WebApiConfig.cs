﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //跨域配置
            var cors = new EnableCorsAttribute("http://localhost:8000,http://localhost:5000", "*", "*");
            config.EnableCors(cors);

            // Web API 設定和服務

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "EmailApi",
                routeTemplate: "api/member/emailvalidate",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
