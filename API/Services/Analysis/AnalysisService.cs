﻿using API.Models;
using System;
using System.Linq;

namespace API.Services
{
    public class AnalysisService
    {
        private rehabEntities db = new rehabEntities();

        #region 計算 分數
        public void CalculateScore(string dt_id)
        {
            bool x = db.score.Any() ? true : false;  //score 是否有資料
            int s_id = x == true ? db.score.Max(p => p.score_id) + 1 : 1;  //score_id

            int part = db.detection.Where(p => p.detection_id == dt_id).FirstOrDefault().par_id;

            double[] rms = db.rms.Where(p => p.detection_id == dt_id).Select(p => p.data).ToArray();
            double[] mf = db.mf.Where(p => p.detection_id == dt_id).Select(p => p.data).ToArray();

            int a = 0;
            if (part == 1)
                a = rms.Count() >= 38 ? 38 : rms.Count();
            else
                a = rms.Count() >= 25 ? 25 : rms.Count();

            double avg_rms = Math.Round(rms.Take(a).Average(), 5);
            double avg_mf = Math.Round(mf.Take(a).Average(), 5);

            double[] score = new double[2]; double temp = 0;

            for (int i = 1; i <= 2; i++)
            {
                temp = (i == 1) ? avg_rms : avg_mf;
                double range1 = db.evaluation.Where(p => p.part == part && p.value == i).FirstOrDefault().range1;  //正常
                double range2 = db.evaluation.Where(p => p.part == part && p.value == i).FirstOrDefault().range2;  //輕度
                double range3 = db.evaluation.Where(p => p.part == part && p.value == i).FirstOrDefault().range3;  //重度

                //分數計算
                if (temp >= range1)
                    score[i - 1] = 100;
                else if (temp == range2)
                    score[i - 1] = 51;
                else if (temp < range1 && temp > range2)
                    score[i - 1] = Math.Round(Spacing_50_100(part, i, temp, range2), 0);
                else if (temp < range2 && temp > range3)
                    score[i - 1] = Math.Round(Spacing_0_50(part, i, temp, range3), 0);
                else if (temp <= range3)
                    score[i - 1] = 0;
            }

            score sc = new score
            {
                score_id = s_id,
                rms = score[0],
                mf = score[1],
                average = Math.Round((score[0] + score[1]) / 2, 0),
                detection_id = dt_id
            };
            db.score.Add(sc);

            db.SaveChanges();
        }
        #endregion

        #region 肌肉萎縮區間 (重度-輕度)
        public double Spacing_0_50(int part, int value, double avg, double min)
        {
            double spacing = db.evaluation.Where(p => p.part == part && p.value == value).FirstOrDefault().spacing_0_50;

            return (avg - min) / spacing;
        }
        #endregion

        #region 潛在肌肉萎縮區間 (輕度-正常人)
        public double Spacing_50_100(int part, int value, double avg, double min)
        {
            double spacing = db.evaluation.Where(p => p.part == part && p.value == value).FirstOrDefault().spacing_50_100;

            return (avg - min) / spacing;
        }
        #endregion
    }
}