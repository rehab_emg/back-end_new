﻿using API.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Services
{
    public class InsertEmgService
    {
        private rehabEntities db = new rehabEntities();

        #region 取得 dt_id (20181013-test01-01-1 / 日期-帳號-今日檢測次數-內側肌||外側肌)
        public string GetDetectionId(string account)
        {
            string dt_id = "";  //dt_id 檢測id
            string tmp_id = DateTime.Now.ToString("yyyyMMdd") + "-" + account + "-";  //ex:20180906-test01-
            bool check = db.detection.Select(p => p.detection_id).ToList().Exists(p => p.Contains(tmp_id));  //dt_id 是否已存在

            //今日是否有檢測過  true -> 流水號+1  false -> 流水號為01
            if (check == true)
            {
                string dt_last = db.detection.Where(p => p.detection_id.Contains(tmp_id)).ToList().Last().detection_id;
                var temp = dt_last.Split('-');

                if (temp[3] == "1")  //股外側肌||腓腸肌(外) 的數據 尚未儲存
                    dt_id = dt_last.Substring(0, dt_last.Length - 1) + "2";
                else
                {
                    int i = int.Parse(temp[2]) + 1;
                    dt_id = (i < 10) ? tmp_id + "0" + i + "-1" : tmp_id + i + "-1";
                }
            }
            else
                dt_id = tmp_id + "01-1";

            return dt_id;
        }
        #endregion

        #region 計算基準線 (使用前測資料)
        public double Baseline(string dt_id)
        {
            List<int> list = db.semg_pre.Where(p => p.detection_id == dt_id).Select(p => p.data).ToList();
            int result = 0;

            for (int i = 0; i < list.Count(); i++)
            {
                result += list[i];
            }
            return Convert.ToDouble(result) / list.Count();
        }
        #endregion

        #region 接收原始資料 sEMG
        public void GetData(SemgData semg, int d)
        {
            DateTime dt_time = new DateTime(1970, 1, 1).AddMilliseconds(semg.dt_time).AddHours(8);  //檢測時間

            for (int a = 0; a < 2; a++)
            {
                bool x; int id = 0; string dt_id = "";
                string temp_data = a == 0 ? semg.data1 : semg.data2;  //data1 內側  data2 外側

                switch (d)  //1 前測  2 檢測  3 後側
                {
                    case 1:
                        x = db.semg_pre.Any() ? true : false;  //semg_pre 是否有資料
                        id = x == true ? db.semg_pre.Max(p => p.semg_id) + 1 : 1;  //semg_id
                        dt_id = GetDetectionId(semg.account);  //取得 檢測id
                        break;
                    case 2:
                        x = db.semg.Any() ? true : false;  //semg 是否有資料
                        id = x == true ? db.semg.Max(p => p.semg_id) + 1 : 1;  //semg_id
                        dt_id = db.detection.Where(p => p.account == semg.account).ToList().Last().detection_id;  //檢測 id (從detection 取)
                        break;
                    case 3:
                        x = db.semg_post.Any() ? true : false;  //semg_post 是否有資料
                        id = x == true ? db.semg_post.Max(p => p.semg_id) + 1 : 1;  //semg_id
                        dt_id = db.detection.Where(p => p.account == semg.account).ToList().Last().detection_id;  //檢測 id (從detection 取)
                        break;
                }
                if (d != 1 && a == 0)
                    dt_id = dt_id.Substring(0, dt_id.Length - 1) + "1";

                //取得 肌肉代碼
                int part = 0;  //部位 (1 股內側肌  2 股外側肌  3 腓腸肌(內)  4 腓腸肌(外))
                if (semg.place == 1)
                    part = dt_id.Substring(dt_id.Length - 1, 1) == "1" ? 1 : 2;
                else
                    part = dt_id.Substring(dt_id.Length - 1, 1) == "1" ? 3 : 4;

                JArray array = JArray.Parse(temp_data);
                int[] data = new int[array.Count()];
                int[] num = new int[Convert.ToInt32(array.Last["count"])];  //每[次]資料的數量
                int[] pos = new int[Convert.ToInt32(array.Last["count"])];  //每[次]資料的第一筆索引值

                int k = 0;
                for (int i = 0; i < array.Count(); i++)
                {
                    data[i] = Convert.ToInt32(array[i]["semg"]);  //data

                    if (i == 0 || Convert.ToInt32(array[i]["count"]) != Convert.ToInt32(array[i - 1]["count"]))
                    {
                        pos[k] = i;
                        k += 1;
                    }

                    if (d == 1)
                    {
                        semg_pre emg = new semg_pre
                        {
                            semg_id = id + i,
                            data = data[i],
                            detection_id = dt_id
                        };
                        db.semg_pre.Add(emg);
                    }
                    else if (d == 2)
                    {
                        semg emg = new semg
                        {
                            semg_id = id + i,
                            data = data[i],
                            count = Convert.ToInt32(array[i]["count"]),
                            detection_id = dt_id
                        };
                        db.semg.Add(emg);
                    }
                    else
                    {
                        semg_post emg = new semg_post
                        {
                            semg_id = id + i,
                            data = data[i],
                            detection_id = dt_id
                        };
                        db.semg_post.Add(emg);
                    }
                }
                List<detection> dtn = new List<detection>();
                if (d == 1)
                {
                    detection dt = new detection
                    {
                        detection_id = dt_id,
                        date = dt_time,
                        isLR = semg.isLR,
                        place_id = semg.place,
                        par_id = part,
                        account = semg.account
                    };
                    db.detection.Add(dt);
                }
                db.SaveChanges();

                if (d == 2)
                {
                    double baseline = Baseline(dt_id);  //計算基準線

                    if (Convert.ToInt32(array.Last["count"]) == 1)  //儲存資料的位置
                        num[0] = data.Length;
                    else
                    {
                        for (int i = 0; i < Convert.ToInt32(array.Last["count"]); i++)
                        {
                            if (i == 0)
                                num[i] = pos[1];
                            else if (i == Convert.ToInt32(array.Last["count"]) - 1)
                                num[i] = data.Length - pos[Convert.ToInt32(array.Last["count"]) - 1];
                            else
                                num[i] = pos[i + 1] - pos[i];
                        }
                    }
                    CalculateRMS(dt_id, data, pos, num, baseline);
                    CalculateiEmg(dt_id, data, baseline);
                }
            }
        }
        #endregion

        #region 計算FFT (暫用)
        public void Calculate(string dt_id)
        {
            int[] data = db.semg.Where(p => p.detection_id == dt_id).Select(p => p.data).ToArray();
            int[] count = db.semg.Where(p => p.detection_id == dt_id).Select(p => p.count).ToArray();
            double baseline = Baseline(dt_id);

            int[] num = new int[count.Last()];  //每[次]資料的數量
            int[] pos = new int[count.Last()];  //每[次]資料的第一筆索引值

            int k = 0;
            for (int i = 0; i < data.Length; i++)
            {
                if (i == 0 || count[i] != count[i - 1])
                {
                    pos[k] = i;
                    k += 1;
                }
            }
            for (int i = 0; i < Convert.ToInt32(count.Last()); i++)
            {
                if (i == 0)
                    num[i] = pos[1];
                else if (i == Convert.ToInt32(count.Last()) - 1)
                    num[i] = data.Length - pos[Convert.ToInt32(count.Last()) - 1];
                else
                    num[i] = pos[i + 1] - pos[i];
            }
            FFTService.FFTOriginalData(data, dt_id, pos, num, baseline);
        }
        #endregion

        #region 計算 RMS
        public void CalculateRMS(string dt_id, int[] data, int[] pos, int[] num, double baseline)
        {
            bool x = db.rms.Any() ? true : false;  //rms 是否有資料
            int r_id = x == true ? db.rms.Max(p => p.rms_id) + 1 : 1;  //rms_id
            double[] result = new double[num.Length];

            //計算 RMS
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i]; j++)
                {
                    result[i] += Math.Pow(data[pos[i] + j] - baseline, 2);  //將(原始肌電-基準線)算出肌電差並平方後加總
                }
                result[i] = Math.Round(Math.Sqrt(result[i] / num[i]), 5);  //再將其開根號後除以窗格長度

                rms rms = new rms
                {
                    rms_id = r_id + i,
                    data = result[i],
                    detection_id = dt_id
                };
                db.rms.Add(rms);
            }
            db.SaveChanges();
        }
        #endregion

        #region 計算 iEMG
        public void CalculateiEmg(string dt_id, int[] data, double baseline)
        {
            bool x = db.iemg.Any() ? true : false;  //iemg 是否有資料
            int i_id = x == true ? db.iemg.Max(p => p.iemg_id) + 1 : 1;  //iemg_id
            double result = 0;

            //計算 iEMG
            for (int i = 0; i < data.Length; i++)
            {
                result += Math.Abs(data[i] - baseline);  //將(原始肌電-基準線)算出肌電差並取絕對值後加總
            }
            result = Math.Round((result / data.Length), 5);  //再將其除以n筆資料

            iemg iemg = new iemg
            {
                iemg_id = i_id,
                data = result,
                detection_id = dt_id
            };
            db.iemg.Add(iemg);

            db.SaveChanges();
        }
        #endregion

        #region 計算 iEMG (窗格)
        public void CalculateiEmg_TEMP(string dt_id, int[] data, int[] pos, int[] num, double baseline)
        {
            bool x = db.iemg.Any() ? true : false;  //iemg 是否有資料
            int i_id = x == true ? db.iemg.Max(p => p.iemg_id) + 1 : 1;  //iemg_id
            double[] result = new double[num.Length];

            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i]; j++)
                {
                    result[i] += Math.Abs(data[i] - baseline);
                }
                result[i] = Math.Round((result[i] / num[i]), 5);
            }
        }
        #endregion
    }
}