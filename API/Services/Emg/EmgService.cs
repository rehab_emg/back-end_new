﻿using API.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.Services
{
    public class EmgService
    {
        private rehabEntities db = new rehabEntities();

        #region 取得User's 最後一次檢測id (回傳 20181022-test01-01-)
        public string GetDetectionId_Last(string account)
        {
            bool x = db.detection.Where(p => p.account == account).Any() ? true : false;  //是否檢測過
            string id = "";

            if (x == true)
                id = db.detection.Where(p => p.detection_id.Contains(account)).ToList().Last().detection_id;
            else
                id = null;

            return id?.Substring(0, id.Length - 1);  //如果id == null -> null 否則 return id (20181022-test01-01-)
        }
        #endregion

        #region 取得User's 檢測id(5次檢測 -> 會有10筆資料)
        public List<string> GetDetectionId(string account, int num)
        {
            bool x = db.detection.Where(p => p.account == account).Any() ? true : false;  //是否檢測過
            List<string> id;

            if (x == true)
                id = db.detection.Where(p => p.detection_id.Contains(account)).Select(p => p.detection_id).ToList();
            else
                id = null;

            if (id != null)
            {
                if (num == 5)
                    id = id.Count() >= 10 ? id.Skip(id.Count() - 10).ToList() : id;
            }
            return id;
        }
        #endregion

        #region 取得User's 檢測id(時間區間)
        public List<string> GetDetectionId_Time(RecordTime data)
        {
            bool x = db.detection.Where(p => p.account == data.account).Any() ? true : false;  //是否檢測過
            List<string> id;

            data.date2 = data.date2.AddDays(1);  //因為date 只到 DAY -> 2018-10-03 上午12:00 所以加1 使它判斷

            if (x == true)
                id = db.detection.Where(p => p.detection_id.Contains(data.account) && p.date >= data.date1 && p.date <= data.date2)
                    .Select(p => p.detection_id).ToList();
            else
                id = null;

            return id;
        }
        #endregion

        #region 取得User's 最後一筆紀錄
        public JArray GetUserData(string account)
        {
            string id = GetDetectionId_Last(account);

            if (id == null)  //沒有檢測過
                return JArray.Parse("[]");
            else
            {
                JArray result = new JArray();
                for (int i = 0; i < 2; i++)
                {
                    string temp_id = i == 0 ? id + "1" : id + "2";  //內側  外側
                    var info = db.detection.Where(x => x.detection_id == temp_id)
                        .Join(db.part, x => x.par_id, y => y.part_id, (x, y) => new { x.isLR, x.date, x.place_id, y.muscle }).FirstOrDefault();

                    JArray rms = new JArray  //取得 rms(Array)
                    {
                        db.rms.Where(p => p.detection_id == temp_id).OrderBy(p => p.rms_id).Select(p => p.data)
                    };
                    double iemg = db.iemg.Where(p => p.detection_id == temp_id).FirstOrDefault().data;  //取得 iEMG
                    JArray mpf = new JArray  //取得 mpf(Array)
                    {
                        db.mpf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mpf_id).Select(p => p.data)
                    };
                    JArray mf = new JArray  //取得 mf(Array)
                    {
                        db.mf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mf_id).Select(p => p.data)
                    };
                    JArray sEmg_a = new JArray  //取得 sEMG(Array)
                    {
                        db.semg.Where(p => p.detection_id == temp_id).OrderBy(p => p.semg_id).Select(p => p.data)
                    };

                    var result_temp = new
                    {
                        isLR = info.isLR == 1 ? "左腳" : "右腳",
                        place = info.place_id == 1 ? "大腿" : "小腿",
                        info.date,
                        part = info.muscle,
                        rms,
                        iemg,
                        mpf,
                        mf,
                        data = sEmg_a
                    };
                    result.Add(JObject.FromObject(result_temp));
                }
                return result;
            }
        }
        #endregion

        #region 取得User's 歷史紀錄(5筆 / 時間區間)
        public JArray GetHistoricalData(List<string> id)
        {
            if (id == null)  //沒有檢測過
                return JArray.Parse("[]");
            else
            {
                JArray result = new JArray();
                for (int i = id.Count() - 1; i >= 0; i--)  //由新資料 -> 舊資料
                {
                    string temp_id = id[i];  //因為 LinQ中無法直接使用c#程式  所以把 id[i]先寫出來
                    var info = db.detection.Where(x => x.detection_id == temp_id)
                              .Join(db.part, x => x.par_id, y => y.part_id, (x, y) => new
                              {
                                  x.isLR,
                                  x.date,
                                  x.place_id,
                                  y.muscle
                              }).FirstOrDefault();  //取得 isLR date part
                    JArray rms = new JArray  //取得 rms(Array)
                    {
                        db.rms.Where(p => p.detection_id == temp_id).OrderBy(p => p.rms_id).Select(p => p.data)
                    };
                    double iemg = db.iemg.Where(p => p.detection_id == temp_id).FirstOrDefault().data;  //取得 iEMG
                    JArray mpf = new JArray  //取得 mpf(Array)
                    {
                        db.mpf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mpf_id).Select(p => p.data)
                    };
                    JArray mf = new JArray  //取得 mf(Array)
                    {
                        db.mf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mf_id).Select(p => p.data)
                    };
                    JArray sEmg_a = new JArray  //取得 sEMG(Array)
                    {
                        db.semg.Where(p => p.detection_id == temp_id).OrderBy(p => p.semg_id).Select(p => p.data)
                    };

                    var result_temp = new
                    {
                        isLR = info.isLR == 1 ? "左腳" : "右腳",
                        place = info.place_id == 1 ? "大腿" : "小腿",
                        info.date,
                        part = info.muscle,
                        rms,
                        iemg,
                        mpf,
                        mf,
                        data = sEmg_a
                    };
                    result.Add(JObject.FromObject(result_temp));
                }
                return result;
            }
        }
        #endregion

        #region 取得User's 全部檢測時間(for Android)
        public JArray GetDetectionTime(string account)
        {
            List<string> id = GetDetectionId(account, 0);

            if (id == null)  //沒有檢測過
                return JArray.Parse("[]");
            else
            {
                JArray result = new JArray();

                int y = id.Count() - 1;
                while (true)
                {
                    string temp_id = id[y].Substring(0, 8);  //取得年月日 20181010
                    int num = id.Count(p => p.Contains(temp_id));
                    int[] temp_isLR = db.detection.Where(p => p.detection_id.Contains(temp_id) && p.account == account).Select(p => p.isLR).ToArray();
                    int[] temp_place = db.detection.Where(p => p.detection_id.Contains(temp_id) && p.account == account).Select(p => p.place_id).ToArray();
                    string[] isLR = new string[temp_isLR.Length / 2];
                    string[] place = new string[temp_place.Length / 2];
                    int x = 0;

                    for (int i = 0; i < temp_isLR.Length; i += 2)
                    {
                        isLR[x] = temp_isLR[i] == 1 ? "左腳" : "右腳";
                        place[x] = temp_place[i] == 1 ? "大腿" : "小腿";
                        x += 1;
                    }

                    var result_temp = new
                    {
                        yy = temp_id.Substring(0, 4),
                        mm = temp_id.Substring(4, 2),
                        dd = temp_id.Substring(6, 2),
                        count = num / 2,
                        isLR,
                        place,
                        account
                    };
                    result.Add(JObject.FromObject(result_temp));

                    if (id.First().Substring(0, 8) == temp_id)
                        break;
                    y -= num;
                }
                return result;
            }
        }
        #endregion

        #region 取得User's 歷史紀錄(由檢測id for Android)
        public JArray GetHistoricalData_dt(DetectionId data)
        {
            JArray result = new JArray();
            for (int i = 0; i < 2; i++)
            {
                string temp_id = i == 0 ? data.dt_id + "-1" : data.dt_id + "-2";  //內側  外側
                var info = db.detection.Where(x => x.detection_id == temp_id)
                    .Join(db.part, x => x.par_id, y => y.part_id, (x, y) => new { x.isLR, x.date, x.place_id, y.muscle }).FirstOrDefault();

                double avg_rms = db.score.Where(p => p.detection_id == temp_id).FirstOrDefault().rms;
                double avg_mf = db.score.Where(p => p.detection_id == temp_id).FirstOrDefault().mf;
                double avg = db.score.Where(p => p.detection_id == temp_id).FirstOrDefault().average;
                JArray rms = new JArray  //取得 rms(Array)
                {
                    db.rms.Where(p => p.detection_id == temp_id).OrderBy(p => p.rms_id).Select(p => p.data)
                };
                double iemg = db.iemg.Where(p => p.detection_id == temp_id).FirstOrDefault().data;  //取得 iEMG
                JArray mpf = new JArray  //取得 mpf(Array)
                {
                    db.mpf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mpf_id).Select(p => p.data)
                };
                JArray mf = new JArray  //取得 mf(Array)
                {
                    db.mf.Where(p => p.detection_id == temp_id).OrderBy(p => p.mf_id).Select(p => p.data)
                };
                JArray sEmg_a = new JArray  //取得 sEMG(Array)
                {
                    db.semg.Where(p => p.detection_id == temp_id).OrderBy(p => p.semg_id).Select(p => p.data)
                };

                var result_temp = new
                {
                    isLR = info.isLR == 1 ? "左腳" : "右腳",
                    place = info.place_id == 1 ? "大腿" : "小腿",
                    info.date,
                    part = info.muscle,
                    avg_rms,
                    avg_mf,
                    avg,
                    rms,
                    iemg,
                    mpf,
                    mf,
                    data = sEmg_a
                };
                result.Add(JObject.FromObject(result_temp));
            }
            return result;
        }
        #endregion

        #region 取得User's 最後一筆的肌肉萎縮程度評估
        public JArray GetEvaluation_Last(string account)
        {
            string id = GetDetectionId_Last(account);

            if (id == null)  //沒有檢測過
                return JArray.Parse("[]");
            else
            {
                JArray result = new JArray();
                for (int i = 0; i < 2; i++)
                {
                    string temp_id = i == 0 ? id + "1" : id + "2";  //內側  外側
                    var rms = db.score.Where(x => x.detection_id == temp_id).FirstOrDefault().rms;
                    var mf = db.score.Where(x => x.detection_id == temp_id).FirstOrDefault().mf;
                    var avg = db.score.Where(x => x.detection_id == temp_id).FirstOrDefault().average;

                    var result_temp = new
                    {
                        rms,
                        mf,
                        avg
                    };
                    result.Add(JObject.FromObject(result_temp));
                }
                return result;
            }
        }
        #endregion

        #region 取得部位檢測次數
        public JArray GetPartFrequency()
        {
            JArray result = new JArray();

            var part = db.part.OrderBy(p => p.part_id).Select(p => p.muscle).ToList();  //取得全部的檢測部位的中文名稱

            for (int i = 1; i <= 4; i++)
            {
                for (int j = 1; j <= 2; j++)
                {
                    int count = db.detection.Where(p => p.isLR == j && p.par_id == i).Count();

                    var result_temp = new
                    {
                        isLR = j == 1 ? "左腳" : "右腳",
                        part = part[i - 1],
                        count
                    };
                    result.Add(JObject.FromObject(result_temp));
                }
            }
            return result;
        }
        #endregion

        #region 取得左右腳檢測次數
        public JObject GetLegFrequency()
        {
            var left = db.detection.Where(p => p.isLR == 1).Count() / 2;
            var right = db.detection.Where(p => p.isLR == 2).Count() / 2;

            var result = new
            {
                left,
                right
            };
            return JObject.FromObject(result);
        }
        #endregion

        #region 取得年齡分布
        public JObject GetAgeFrequency()
        {
            var birth = db.member.Where(p => string.IsNullOrEmpty(p.authcode)).Select(p => p.birthday).ToList();  //if authcode !=null OR empty 則不計入
            string nowYear = DateTime.Now.Year.ToString();  //今年年份
            int[] num = new int[5];
            JObject result = new JObject();

            for (int i = 0; i <= birth.Count() - 1; i++)
            {
                string year = birth[i].ToString().Substring(0, 4);  //取得出生年
                int age = int.Parse(nowYear) - int.Parse(year);  //計算年齡

                if (age >= 0 && age < 20)  //0~19
                    num[0] += 1;
                else if (age >= 20 && age < 40)  //20~39
                    num[1] += 1;
                else if (age >= 40 && age < 60)  //40~59
                    num[2] += 1;
                else if (age >= 60 && age < 70)  //60~69
                    num[3] += 1;
                else  //70up
                    num[4] += 1;
            }

            int x = 0;
            string charAge = "";
            for (int i = 0; i <= 4; i++)
            {
                if (i <= 2)
                {
                    charAge = x + "~" + (x + 19) + "歲";
                    x += 20;
                }
                else if (i == 3)
                {
                    charAge = x + "~" + (x + 9) + "歲";
                    x += 10;
                }
                else
                    charAge = "70歲以上";

                JArray array = new JArray
                {
                    num[i],
                    Math.Round(Convert.ToDouble(num[i]) / birth.Count(), 2)
                };
                result.Add(charAge, array);
            }
            return result;
        }
        #endregion

        #region 取得性別分布
        public JObject GetSexFrequency()
        {
            var sex = db.member.GroupBy(p => p.sex).Select(p => new
            {
                sex = p.Key,
                count = p.Count()
            }).ToList();

            int[] num = new int[2];
            JObject result = new JObject();

            if (sex.Count() == 1)
            {
                if (sex[0].sex == 1)
                {
                    num[0] = sex[0].count;
                    num[1] = 0;
                }
                else
                {
                    num[0] = 0;
                    num[1] = sex[0].count;
                }
            }
            else if (sex.Count() == 2)
            {
                num[0] = sex[0].count;
                num[1] = sex[1].count;
            }

            for (int i = 1; i <= 2; i++)
            {
                result.Add(i == 1 ? "男" : "女", num[i - 1]);
            }
            return result;
        }
        #endregion
    }
}