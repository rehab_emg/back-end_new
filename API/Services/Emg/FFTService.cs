﻿using API.Models;
using System;

namespace API.Services
{
    public class FFTService
    {
        private static readonly rehabEntities db = new rehabEntities();

        #region 轉換 sEMG
        public static void FFTOriginalData(int[] data, string dt_id, int[] pos, int[] num, double baseline)
        {
            int power = 0;  //次方
            if ((data.Length & (data.Length - 1)) != 0)  //假設data.Length=16(以2進位的方式計算) -> 10000 AND 01111 == 0
            {
                while (true)  //比較data.Length 與2的次方哪個比較大
                {
                    if ((data.Length - Math.Pow(2, power)) < 0)
                        break;

                    power += 1;
                }
            }
            else
                power = Convert.ToInt32(Math.Sqrt(data.Length));

            //把int[] semg -> double[] semg 並將data.Length 湊成2的次方 空缺部分補0
            double[] semg_double = new double[Convert.ToInt32(Math.Pow(2, power))];
            for (int i = 0; i <= Convert.ToInt32(Math.Pow(2, power)) - 1; i++)
            {
                if (i >= data.Length)
                    semg_double[i] = 0.0;
                else
                    semg_double[i] = Convert.ToDouble(data[i]) - baseline;
            }
            CalculateFFT(semg_double, Convert.ToInt32(Math.Pow(2, power)), dt_id, pos, num);
        }
        #endregion

        #region 計算 FFT (其計算的個數只能為2的n次方)
        public static void CalculateFFT(double[] input, int n, string dt_id, int[] pos, int[] num)
        {
            //輸入序列只有一個元素，輸出這個元素並返回
            int length = n;  //輸入序列的長度
            int half = length / 2;  //輸入序列長度的一半
            Complex[] output = new Complex[length];  //有輸入序列的長度確定輸出序列的長度

            double[] evens = new double[half];  //序列中下標為偶數的點
            for (int i = 0; i < half; i++)
            {
                evens[i] = input[2 * i];
            }
            Complex[] evenResult = DFT(evens, half);  //偶數列的DFT

            double[] odds = new double[half];  //序列中下標為奇數的點
            for (int i = 0; i < half; i++)
            {
                odds[i] = input[2 * i + 1];
            }
            Complex[] oddResult = DFT(odds, half);  //奇數列的DFT

            Complex w1;
            w1 = Omega(n);  //ω=exp（j*2*pi/n）n為信號長度
            Complex[] w = new Complex[n];  //儲存w N次方的陣列
            for (int i = 0; i < n; i++)
            {
                w[i] = Complex.Powcc(w1, i);
            }
            for (int k = 0; k < half; k++)
            {
                //進行蝶形運算
                Complex oddPart = Complex.Commul(oddResult[k], w[k]);
                output[k] = Complex.ComSum(evenResult[k], oddPart);  
                output[k].a = Math.Round(output[k].a, 5);
                output[k].b = Math.Round(output[k].b, 5);

                output[k + half] = Complex.Decrease(evenResult[k], oddPart);
                output[k + half].a = Math.Round(output[k + half].a, 5);
                output[k + half].b = Math.Round(output[k + half].b, 5);
            }
            FSService.CalculateAmplitude(output, n, dt_id, pos, num);
        }
        #endregion

        #region 計算 Omega
        public static Complex Omega(int n)
        {
            Complex x;
            x.a = Math.Cos(0 - 2 * Math.PI / n);  //cos(0-2*pi/n)
            x.b = Math.Sin(0 - 2 * Math.PI / n);  //sin(0-2*pi/n)
            return x;
        }
        #endregion

        #region 計算 DFT
        public static Complex[] DFT(double[] signal, int n)  //(semg[](信號) / semg.Length(信號長度))
        {
            int i, j;
            Complex w1;
            w1 = Omega(n);  //ω=exp（j*2*pi/n）(旋轉因子) n為信號長度
            Complex[] w = new Complex[n];  //儲存w N次方的陣列

            for (i = 0; i < n; i++)
            {
                w[i] = Complex.Powcc(w1, i);
            }

            Complex[] f = new Complex[n];
            Complex temp;  //w[i]的次方
            Complex temp1; //f中單項的值
            for (i = 0; i < n; i++)
            {
                f[i].a = 0;
                f[i].b = 0;
                for (j = 0; j < n; j++)
                {
                    temp = Complex.Powcc(w[i], j);
                    temp1 = Complex.Commul(signal[j], temp);
                    f[i] = Complex.ComSum(f[i], temp1);
                    f[i].a = Math.Round(f[i].a, 5);
                    f[i].b = Math.Round(f[i].b, 5);
                }
            }
            return f;
        }
        #endregion
    }
}