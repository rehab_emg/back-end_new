﻿using API.Models;
using System;
using System.Linq;

namespace API.Services
{
    public class FSService
    {
        private static rehabEntities db = new rehabEntities();

        #region 計算 Amplitude (振幅 =>即 FFT的絕對值)
        public static void CalculateAmplitude(Complex[] x, int n, string dt_id, int[] pos, int[] num)
        {
            bool y = db.amplitude.Any() ? true : false;  //amplitude 是否有資料
            int a_id = y == true ? db.amplitude.Max(p => p.amp_id) + 1 : 1;  //amp_id
            double[] amp = new double[pos[pos.Length - 1] + num[num.Length - 1]];

            for (int i = 0; i < pos[pos.Length - 1] + num[num.Length - 1]; i++)
            {
                amp[i] = Math.Round(Math.Sqrt(Math.Pow(x[i].a, 2) + Math.Pow(x[i].b, 2)), 5);
            }

            for (int i = 0; i < pos[pos.Length - 1] + num[num.Length - 1]; i++)
            {
                amplitude ampl = new amplitude
                {
                    amp_id = a_id + i,
                    data = amp[i],
                    detection_id = dt_id
                };
                db.amplitude.Add(ampl);
            }
            db.SaveChanges();

            CalculateMPF(amp, pos[pos.Length - 1] + num[num.Length - 1], dt_id, pos, num);
            CalculateMF(amp, pos[pos.Length - 1] + num[num.Length - 1], dt_id, pos, num);
        }
        #endregion

        #region 計算 MPF
        public static void CalculateMPF(double[] amp, int n, string dt_id, int[] pos, int[] num)  //x -> FFT結果 / n -> 取樣個數(排除補0)
        {
            bool y = db.mpf.Any() ? true : false;  //mpf 是否有資料
            int m_id = y == true ? db.mpf.Max(o => o.mpf_id) + 1 : 1;  //mpf_id
            double fs = 1 / 0.05;  //取樣頻率 => 次/s
            double[] f = new double[n];  //頻率
            double[] p = { 0, 0 };
            double[] sum = new double[num.Length];
            double[] m1 = new double[num.Length];
            double[] m2 = new double[num.Length];
            double df = fs / n;  //df -> delta 頻率 => 取樣頻率/取樣個數

            //周期圖法功率譜 => amp平方/n (振幅的平方/取樣個數)
            for (int i = 0; i < num.Length; i++)
            {
                double[] power = new double[num[i]];  //週期圖法功率譜
                p[0] = 0; p[1] = 0;

                for (int j = 0; j < num[i]; j++)
                {
                    f[pos[i] + j] = i * df;  //時間*每秒採樣次數
                    power[j] = Math.Pow(amp[pos[i] + j], 2) / num[i];  //FFT運算後的值並取絕對值(振幅)再平方除以窗格長度
                    sum[i] += power[j];
                }
                m2[i] = sum[i];

                for (int k = 0; k < num[i] - 1; k++)
                {
                    p[0] += power[k];
                }
                for (int m = 1; m < num[i]; m++)
                {
                    p[1] += power[m];
                }
                m1[i] = df * p[0] + p[1];
            }

            for (int i = 0; i < num.Length; i++)
            {
                mpf mpf = new mpf
                {
                    mpf_id = m_id + i,
                    data = Math.Round(m1[i] / m2[i], 5),
                    detection_id = dt_id
                };
                db.mpf.Add(mpf);
            }
            db.SaveChanges();
        }
        #endregion

        #region 計算 MF
        public static void CalculateMF(double[] amp, int n, string dt_id, int[] pos, int[] num)  //x -> FFT結果 / n -> 取樣個數(排除補0)
        {
            bool y = db.mf.Any() ? true : false;  //mf 是否有資料
            int m_id = y == true ? db.mf.Max(p => p.mf_id) + 1 : 1;  //mf_id
            double fs = 1 / 0.05;  //取樣頻率 => 次/s
            double[] f = new double[n];  //頻率
            double[] sum = new double[num.Length];
            double[] result = new double[num.Length];

            //週期圖法功率譜 => amp平方/n (振幅的平方/取樣個數)

            for (int i = 0; i < num.Length; i++)
            {
                double[] power = new double[num[i]];  //週期圖法功率譜
                for (int j = 0; j < num[i]; j++)
                {
                    f[pos[i] + j] = i * (fs / n);  //時間*每秒採樣次數
                    power[j] = Math.Pow(amp[pos[i] + j], 2) / num[i];  //FFT運算後的值並取絕對值(振幅)再平方除以窗格長度
                    sum[i] += power[j];
                }
                result[i] = Math.Round(0.5 * sum[i], 5);
            }

            for (int i = 0; i < num.Length; i++)
            {
                mf mf = new mf
                {
                    mf_id = m_id + i,
                    data = result[i],
                    detection_id = dt_id
                };
                db.mf.Add(mf);
            }
            db.SaveChanges();
        }
        #endregion
    }
}