﻿namespace API.Services
{
    //定義複數
    public struct Complex
    {
        //複數 a + bi 中
        public double a;  //a為實部
        public double b;  //b為虛部

        //實數和複數相乘
        public static Complex Commul(double x, Complex y)
        {
            Complex c = new Complex
            {
                a = x * y.a,
                b = x * y.b
            };
            return c;
        }

        //複數和複數相乘  重載上一個函數
        public static Complex Commul(Complex x, Complex y)
        {
            Complex c = new Complex
            {
                a = x.a * y.a - x.b * y.b,
                b = x.a * y.b + x.b * y.a
            };
            return c;
        }

        //複數和複數相加
        public static Complex ComSum(Complex x, Complex y)
        {
            Complex c = new Complex
            {
                a = x.a + y.a,
                b = x.b + y.b
            };
            return c;
        }

        //實數和複數相加
        public static Complex ComSum(double x, Complex y)
        {
            Complex c = new Complex
            {
                a = x + y.a,
                b = y.b
            };
            return c;
        }

        //複數和複數相減
        public static Complex Decrease(Complex x, Complex y)
        {
            Complex c = new Complex
            {
                a = x.a - y.a,
                b = x.b - y.b
            };
            return c;
        }

        //實數與複數相減
        public static Complex Decrease(double x, Complex y)
        {
            Complex c = new Complex
            {
                a = x - y.a,
                b = 0 - y.b
            };
            return c;
        }

        //複數的遞迴相乘求複數的N次方
        public static Complex Powcc(Complex x, double n)
        {
            int k;
            Complex xout;
            xout.a = 1;
            xout.b = 0;
            for (k = 1; k <= n; k++)
            {
                xout = Commul(xout, x);
            }
            return xout;
        }
    }
}