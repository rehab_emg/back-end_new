﻿using API.Models;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace API.Services
{
    public class MemberService
    {
        private rehabEntities db = new rehabEntities();

        #region 取得 UserInfo
        public JObject getById(string account)
        {
            var info = db.member.Where(p => p.account == account).FirstOrDefault();

            var result = new
            {
                info.account,
                info.name,
                info.birthday,
                info.cellphone,
                info.email,
                info.sex,
                info.img
            };
            return JObject.FromObject(result);
        }
        #endregion

        #region 登入
        public bool Login(string account, string password)
        {
            member loginMember = db.member.Where(p => p.account == account && p.password == password).FirstOrDefault();

            if (loginMember != null && string.IsNullOrEmpty(loginMember.authcode))
            {
                if (account == loginMember.account && password == loginMember.password)
                    return true;
            }
            return false;
        }
        #endregion

        #region 檢查是否有重複帳號
        public bool AccountCheck(string account)
        {
            var check = db.member.Where(p => p.account == account).Count();
            bool result = (check == 0);

            return result;
        }
        #endregion

        #region 檢查信箱是否已驗證
        public bool AuthcodeCheck(string account)
        {
            var check = db.member.Where(p => p.account == account).Select(p => p.authcode).FirstOrDefault();
            bool result = (string.IsNullOrEmpty(check));

            return result;
        }
        #endregion

        #region 註冊
        public void Register(member data)
        {
            data.password = HashPassword(data.password);
            db.member.Add(data);
            db.SaveChanges();
        }
        #endregion

        #region 檢查輸入的舊密碼與資料庫裡的是否相同
        public bool CheckPassword(string account, string old)
        {
            member checkMember = db.member.Where(p => p.account == account).FirstOrDefault();
            string check = checkMember.password;

            if (HashPassword(old) == check)
                return true;

            return false;
        }
        #endregion

        #region 修改

        #region 修改密碼
        public void ChangePassword(string account, string newPassword)
        {
            member changeMember = db.member.Where(p => p.account == account).FirstOrDefault();

            if (changeMember != null)
            {
                changeMember.password = HashPassword(newPassword);
                db.SaveChanges();
            }
        }
        #endregion

        #region 修改大頭貼
        public int ChangeImage(MemberChange data)
        {
            var temp = (!string.IsNullOrEmpty(data.buffer)) ? data.buffer.Split(',') : null;  //切割圖片base64 不要','前面的值
            var buffer = (temp != null) ? Convert.FromBase64String(temp[1]) : null;  //圖片base64
            int maxSize = 1 * 1024 * 1024;  //maxSize -> 1MB
            string ext = (buffer != null) ? Path.GetExtension(data.fileName) : null;  //副檔名
            string oldFileName = db.member.Where(p => p.account == data.account).Select(p => p.img).FirstOrDefault();  //舊有資料
            string newFileName = (ext != null) ? data.account + Path.GetExtension(data.fileName) : null;  //新檔名

            if (!string.IsNullOrEmpty(oldFileName))  //如有舊檔則先刪除
                File.Delete(System.Web.Hosting.HostingEnvironment.MapPath("~/A" + oldFileName.Substring(1)));

            if (buffer != null)  //如果會員有上傳照片
            {
                bool check = buffer.Length > maxSize || ((ext != ".png") && (ext != ".jpg") && (ext != ".jpeg"));

                if (check == true)
                    return 401;  //圖片大小過大 or 副檔名不合
                else
                {
                    string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Assets/"), newFileName);
                    File.WriteAllBytes(path, buffer);
                }
            }
            ChangeOther(data.account, data.name, data.cellphone, data.email, (buffer != null) ? "assets/" + newFileName : null);
            return 200;
        }
        #endregion

        #region 修改姓名 電話 Email 大頭貼 (Save 至 DB)
        public void ChangeOther(string account, string name, string cellphone, string email, string path)
        {
            member changeMember = db.member.Where(p => p.account == account).FirstOrDefault();

            if (changeMember != null)
            {
                if (name != changeMember.name)
                    changeMember.name = name;

                if (cellphone != changeMember.cellphone)
                    changeMember.cellphone = cellphone;

                if (email != changeMember.email)
                    changeMember.email = email;

                if (path != null)
                    changeMember.img = path;
                else
                    changeMember.img = null;

                db.SaveChanges();
            }
        }
        #endregion

        #region 修改姓名 電話 Email For APP (Save 至 DB)
        public void ChangeOther_APP(MemberChange_APP data)
        {
            member changeMember = db.member.Where(p => p.account == data.account).FirstOrDefault();

            if (changeMember != null)
            {
                if (data.name != changeMember.name)
                    changeMember.name = data.name;

                if (data.cellphone != changeMember.cellphone)
                    changeMember.cellphone = data.cellphone;

                if (data.email != changeMember.email)
                    changeMember.email = data.email;

                db.SaveChanges();
            }
        }
        #endregion

        #endregion

        #region 重設密碼
        public bool ResetPassword(string authcode, string password)
        {
            member resetMember = db.member.Where(p => p.authcode == authcode).FirstOrDefault();

            if (resetMember != null)
            {
                resetMember.password = HashPassword(password);
                resetMember.authcode = string.Empty;
                db.SaveChanges();

                return true;
            }
            return false;
        }
        #endregion

        #region 忘記密碼 Account Email AuthCode 檢查
        public bool ForgetPasswordCheck(string account, string email, string authcode)
        {
            member checkMember = db.member.Where(p => p.account == account && p.email == email).FirstOrDefault();

            if (checkMember != null && string.IsNullOrEmpty(checkMember.authcode))
            {
                checkMember.authcode = authcode;
                db.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region 信箱驗證
        public bool EmailValidate(string account, string authcode, int x)
        {
            if (x == 1)
            {
                account = UrlDecode(account);
                authcode = UrlDecode(authcode);
            }

            member checkMember = db.member.Where(p => p.account == account && p.authcode == authcode).FirstOrDefault();

            if (checkMember != null)
            {
                checkMember.authcode = string.Empty;
                db.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion

        #region Hash 密碼 (SHA1)
        public string HashPassword(string password)
        {
            string saltkey = "fdsafdfs113fdsa123ta";
            string saltAndPassword = string.Concat(password, saltkey);

            SHA1CryptoServiceProvider sha1Hasher = new SHA1CryptoServiceProvider();

            byte[] passwordData = Encoding.Default.GetBytes(saltAndPassword);
            byte[] hashData = sha1Hasher.ComputeHash(passwordData);

            string result = "";

            for (int i = 0; i < hashData.Length; i++)
            {
                result += hashData[i].ToString("x2");
            }
            return result;
        }
        #endregion

        #region URL參數加解密 (AES MD5)

        //加解密參數為 Byte[] 所以要將字串轉為Byte[]
        byte[] key = Encoding.UTF8.GetBytes("11111111");  //金鑰
        byte[] iv = Encoding.UTF8.GetBytes("99999999");  //向量

        #region 加密
        public string UrlEncode(string str)
        {
            byte[] enStr = Encoding.UTF8.GetBytes(str);

            //IV Key 有固定的長度，用雜湊過後的值可以是同樣的長度
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] enKey = md5.ComputeHash(key);
            byte[] enIv = md5.ComputeHash(iv);

            //AES加解密演算法
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            ICryptoTransform AES = aes.CreateEncryptor(enKey, enIv);

            byte[] result = AES.TransformFinalBlock(enStr, 0, enStr.Length);

            return Convert.ToBase64String(result);
        }
        #endregion

        #region 解密
        public string UrlDecode(string str)
        {
            byte[] deStr = Convert.FromBase64String(str);

            //IV Key 有固定的長度，用雜湊過後的值可以是同樣的長度
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] deKey = md5.ComputeHash(key);
            byte[] deIv = md5.ComputeHash(iv);

            //AES加解密演算法
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            ICryptoTransform AES = aes.CreateDecryptor(deKey, deIv);

            byte[] result = AES.TransformFinalBlock(deStr, 0, deStr.Length);

            return Encoding.UTF8.GetString(result);
        }
        #endregion

        #endregion
    }
}