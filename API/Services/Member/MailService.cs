﻿using System;
using System.Linq;
using System.Net.Mail;

namespace API.Services
{
    public class MailService
    {
        #region 產生AuthCode
        public string GetValidateCode(bool x)  //true --> 信箱驗證信(10碼)  false --> 重設密碼信/信箱驗證信(APP)(6碼)
        {
            string[] Code = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U",
                              "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f",
                              "g", "h", "i", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

            string validateCode = string.Empty;
            int num = (x == true) ? 10 : 6;

            Random rd = new Random();

            for (int i = 1; i <= num; i++)
            {
                validateCode += Code[rd.Next(Code.Count())];
            }
            return validateCode;
        }
        #endregion

        #region 將UserInfo 填入驗證信範本中
        public string GetMailBody(string temp, string account, string authcode, string url, bool x)  //true --> 信箱驗證信  false --> 重設密碼信
        {
            if (x == false)
                temp = temp.Replace("{{AuthCode}}", authcode);

            if (url != "")
                temp = temp.Replace("{{ValidateUrl}}", url);

            temp = temp.Replace("{{UserName}}", account);

            return temp;
        }
        #endregion

        #region 寄信
        public void SendEmail(string email, string mailBody, bool x)  //true --> 信箱驗證信  false --> 重設密碼信
        {
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("emgrehab2018@gmail.com", "rehab123456");
            smtpServer.EnableSsl = true;
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress("emgrehab2018@gmail.com", "emg_rehab團隊");
            mail.To.Add(email);
            mail.Subject = (x == true) ? "會員註冊驗證信" : "申請密碼重設";
            mail.Body = mailBody;
            mail.IsBodyHtml = true;
            smtpServer.Send(mail);
        }
        #endregion
    }
}