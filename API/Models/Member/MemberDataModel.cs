﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    public class Account
    {
        [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,20}$")]
        public string account { get; set; }
    }

    public class MemberLogin : Account
    {
        [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,50}$")]
        public string password { get; set; }
    }

    public class MemberPassword : MemberLogin
    {
        [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,50}$")]
        public string newPassword { get; set; }
    }

    public class MemberChange : MemberChange_APP
    {
        public string buffer { get; set; }
        public string fileName { get; set; }
    }

    public class MemberChange_APP : Account
    {
        public string name { get; set; }
        [RegularExpression(@"^[09]{2}[0-9]{8}$")]
        public string cellphone { get; set; }
        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$")]
        public string email { get; set; }
    }

    public class MemberForget : Account
    {
        [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$")]
        public string email { get; set; }
    }

    public class MemberReset
    {
        [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,16}$")]
        public string password { get; set; }
        public string authcode { get; set; }
    }

    public class MemberValidate_APP : Account
    {
        public string authcode { get; set; }
    }
}