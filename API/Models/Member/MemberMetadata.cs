﻿using System.ComponentModel.DataAnnotations;

namespace API.Models
{
    [MetadataType(typeof(MemberMetadata))]

    public partial class member
    {
        public class MemberMetadata
        {
            [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,20}$")]
            public string account { get; set; }

            [RegularExpression(@"^(?=.*\d)(?=.*[a-zA-Z]).{6,50}$")]
            public string password { get; set; }

            [RegularExpression(@"^[09]{2}[0-9]{8}$")]
            public string cellphone { get; set; }

            [RegularExpression(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$")]
            public string email { get; set; }
        }
    }
}