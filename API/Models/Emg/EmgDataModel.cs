﻿using System;

namespace API.Models
{
    public class SemgData : Account
    {
        public long dt_time { get; set; }  //檢測時間
        public int isLR { get; set; }  // 1 => 左腳  2 => 右腳
        public int place { get; set; }  // 1 => 大腿  2 => 小腿
        public string data1 { get; set; }  //內側
        public string data2 { get; set; }  //外側
    }

    public class RecordTime : Account
    {
        public DateTime date1 { get; set; }
        public DateTime date2 { get; set; }
    }

    public class DetectionId
    {
        public string dt_id { get; set; }
    }
}